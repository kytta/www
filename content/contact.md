---
title: Contact me
template: orphan.html
---

Ways of reaching out to me, sorted by the probability of me responding promptly (the fastest response on top):

- [@NikitaKaramov](https://t.me/NikitaKaramov) on Telegram
- email <me@kytta.dev>
  - PGP key: `0xD397E9BE9E6898FA` ([armoured](https://keys.openpgp.org/vks/v1/by-fingerprint/AF6C280C2A34D3F39BED9366D397E9BE9E6898FA), [Keyoxide](https://keyoxide.org/af6c280c2a34d3f39bed9366d397e9be9e6898fa))
  - to force SSL/TLS encryption: <nikita@secure.mailbox.org>
- [@y0100973:matrix.tu-bs.de](https://matrix.to/#/@y0100973:matrix.tu-bs.de) on Matrix
- [`DMZKXPNM`](https://threema.id/DMZKXPNM) on Threema
- [kytta@404.city](xmpp:kytta@404.city) on XMPP; OMEMO fingerprints:
  - `e5111425 bc365b43 3e6bf9f0 76a4031f 8ded0c29 3dbf50de 83cef216 239a1f44`
  - `a7ad08d6 69cdabc6 81a033e3 471dca05 4544d515 7710fe4b c2d5489d 7514e758`
- `kytta` on [Libera.Chat](ircs://irc.libera.chat:6697)

Links to other social networks and points of presence:

- <a href="https://fosstodon.org/@kytta" rel="me">@kytta</a> on Fosstodon
- [@nikita.karamov](https://www.instagram.com/nikita.karamov/) on Instagram
- [@KyttaWasHere](https://twitter.com/KyttaWasHere) on Twitter (inactive)
